class App {
  String appname;
  String sector;
  String developer;
  int yearwon;

  App(this.appname, this.sector, this.developer, this.yearwon);

  void describe() {
    print(
        "App name: $appname\nSector: $sector \ndeveloper: $developer \nYear:$yearwon");
  }

  void toCapital() {
    print(appname.toUpperCase());
  }
}

void main() {
  App myapp =
      App("Ambani Africa", "Educational Solution", "Mukundi Lambani", 2021);

  myapp.describe();
  myapp.toCapital();
}
